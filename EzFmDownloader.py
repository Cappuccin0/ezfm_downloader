#-*- coding: utf-8 -*-
__author__ = 'hong.gao'

import urllib2, sys

def geturl(y,m,d):
    filename = "ezm%02d%02d%02d.mp3" % (y-2000, m, d)
    return filename, "http://mod.cri.cn/eng/ez/morning/%04d/%s" % (y, filename)

def checkIfExist(path):
    #path = "http://mod.cri.cn/eng/ez/morning/2014/ezm%s.mp3" % filename
    import urllib2
    flag = False
    try:
        content = urllib2.urlopen(path).read(10)
        flag = True
    except:
        pass
    return flag

def get_file_size(url, proxy=None):
    opener = urllib2.build_opener()
    if proxy:
        if url.lower().startswith('https://'):
            opener.add_handler(urllib2.ProxyHandler({'https' : proxy}))
        else:
            opener.add_handler(urllib2.ProxyHandler({'http' : proxy}))
    request = urllib2.Request(url)
    request.get_method = lambda: 'HEAD'
    try:
        response = opener.open(request)
        response.read()
    except Exception, e:
        #print '%s %s' % (url, e)
        return '0'
    else:
        return dict(response.headers).get('content-length', 0)

def generateDate():
    shift = 0
    from datetime import datetime, date, timedelta
    while True:
        d = date.today() - timedelta(days=shift)
        shift += 1
        if d.weekday() < 5:
            yield d.year, d.month, d.day, shift

def downloadRecent():
    import os.path
    print "*" * 50
    print "*%sFYX Downloader%s*" % (" " * 17, " " * 17)
    print "*" * 50
    for y,m,d,idx in generateDate():
        if idx > 8: break
        #print y,m,d, "exist:", checkIfExist(geturl(y,m,d))
        filename, url = geturl(y,m,d)
        filesize = int(get_file_size(url))
        print "Checking: %s" % filename,  "remotesize: %dkb" % (filesize/1024),# "localsize:%d" % (filesize, os.path.getsize(filename))

        if not filesize:
            print "Remote file error, skip"
            continue

        if os.path.exists(filename):
            if filesize <= 1.1 * os.path.getsize(filename):
                print "local exist, ignored"
                continue
            else:
                print "local exist, redownloading"
        else:
            print "start downloading..."
        try:
            f = open(filename, "wb")
            conn = urllib2.urlopen(url)
            dlen = 0
            p1, p2 = -1, 0
            #sys.stdout.write("\t[")
            while True:
                if p2 != p1:
                    sys.stdout.write("\t[%s%s] %i%% %dkb\r" % ("#" * (p2/2), "-" * (50-(p2/2)), p2, dlen/1024))
                    sys.stdout.flush()
                    p1 = p2
                buf = conn.read(8964)
                if not len(buf): break
                dlen += len(buf)
                p2 = dlen * 100 / filesize
                f.write(buf)
        except Exception, e:
            print "error occured:", e
        finally:
            f.close()
        print ""
    print "Done!"


def main():
    downloadRecent()

if __name__ == "__main__":
    main()
    import os
    os.system('pause')
